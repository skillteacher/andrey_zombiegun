using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    public delegate void Massage();
    public event Massage AnimationAttackMomment;

    public void AnimAttack()
    {
        AnimationAttackMomment?.Invoke();
    }
}
