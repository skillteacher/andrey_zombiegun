using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float range = 100f;
    [SerializeField] private float delay = 0.2f;
    [SerializeField] private ParticleSystem muzzleFlashEffect;
    [SerializeField] private GameObject sparksEffect;
    [SerializeField] private float sparksLifeTime = 0.1f;

    private bool isReadyToShoot = true;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && isReadyToShoot)
        {
            Shoot();
        } 
    }
    private void Raycasting()
    {
        RaycastHit hit;
        if (!Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, range)) return;
        //Debug.Log("� ����� �" + hit.transform.name);
        SummonSparks(hit.point);
        Health targetHealth = hit.transform.GetComponent<Health>();
        if (targetHealth == null) return;
        targetHealth.TakeDamage(damage);
    }

    private void Shoot()
    {
        PlayMuzzleFlash();
        Raycasting();
        StartCoroutine(DelayCountdown(delay));
    }

    private void PlayMuzzleFlash()
    {
        muzzleFlashEffect.Play();
    }

    private IEnumerator DelayCountdown(float delay)
    {
        isReadyToShoot = false;
        yield return new WaitForSeconds(delay);
        isReadyToShoot = true;
    }

    private void SummonSparks(Vector3 point)
    {
         GameObject sparks = Instantiate(sparksEffect, point, Quaternion.identity);
        Destroy(sparks, sparksLifeTime);
    }
 }  
