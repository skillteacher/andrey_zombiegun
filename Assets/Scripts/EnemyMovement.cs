using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyMovement : MonoBehaviour
{
    private Transform target;
    [SerializeField] private float visionRange = 10f;
    [SerializeField] private NavMeshAgent NavMeshAgent;
    [SerializeField] private float AttackDelay = 1f;
    [SerializeField] private Animator animator;
    private bool isRaged;
    private float DistanceToTarget;
    private float timer;

    private void Awake()
    {
        NavMeshAgent = GetComponent<NavMeshAgent>();
        target = TargetForEnemy.Instance.GetTarget();

    }

    private void Update()
    {
        DistanceToTarget = Vector3.Distance(target.position, transform.position);

        if (isRaged)
        {
            EngageTarget();
        }
        else if (visionRange >= DistanceToTarget)
        {
            isRaged = true;
            animator.SetBool("move", true);
        }
    }

    private void EngageTarget()
    {
        if (NavMeshAgent.stoppingDistance >= DistanceToTarget)
        {
            AttackTarget();
        }
        else
        {
            ChaseTarget();
        }
    }

    private void ChaseTarget()
    {
        NavMeshAgent.SetDestination(target.position);
        animator.SetBool("attack", false);
    }

    private void AttackTarget()
    {
        //Debug.Log(name + " Attack " + target.name);
        timer += Time.deltaTime;
        if (timer <= AttackDelay) return;
        timer = 0f;
        animator.SetBool("attack", true);
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, visionRange);
    }
}


