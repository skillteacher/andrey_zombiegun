using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private float damage = 2f;
    [SerializeField] private EnemyAnimation enemyAnimation;
    [SerializeField] private Transform target;

    private void Awake()
    {
        target = TargetForEnemy.Instance.GetTarget();
    }

    private void OnEnable()
    {
        enemyAnimation.AnimationAttackMomment += AttackTarget;
    }

    private void OnDisable()
    {
        enemyAnimation.AnimationAttackMomment -= AttackTarget;
    }


    public void AttackTarget()
    {
        target.GetComponent<PlayerHealth>().TakeDamage(damage);
    } 
}
